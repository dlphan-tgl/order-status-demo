'use client';

import { Button, Row, Space, Steps } from 'antd';
import Link from 'next/link';
import { useState } from 'react';
import { LoadingOutlined, SmileOutlined, SolutionOutlined, UserOutlined } from '@ant-design/icons';
import dayjs from 'dayjs';

const description = 'This is a description.';

export default function Home() {
  const [status, setStatus] = useState(0);

  const items = [
    {
      title: '荷物受付',
      description: dayjs().format('YYYY/MM/DD HH:mm:ss')
    },
    {
      title: '通関完了',
      description
    },
    {
      title: '通関検査中',
      description
    },
    {
      title: '飛行機出発',
      description
    },
    {
      title: '荷物が空港に到着',
      description
    },
    {
      title: '発注済み',
      description
    },
    {
      title: 'ピック待ち',
      description
    },
    {
      title: '支払済み',
      description
    },
    {
      title: '注文済み',
      description
    }
  ];

  const [listStatus, setListStatus] = useState<any>(items);

  return (
    <div
      className="App"
      style={{
        margin: 'auto',
        padding: '2rem',
        // width: 600,
        display: 'flex',
        // flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
      }}
    >
      <Row>
        {/* <Button
          type="primary"
          style={{ marginBottom: '1rem', marginRight: '1rem' }}
          onClick={() => {
            if (status > 0) {
              setStatus(status - 1);
              // const begin = listStatus.slice(0, status);
              // const curr = {
              //   ...listStatus[status],
              //   description: description
              // };
              // const end = listStatus.slice(status);

              // setListStatus([...begin, curr, ...end]);
            }
          }}
        >
          Previous status
        </Button> */}
        <Button
          disabled={status === 8}
          type="primary"
          style={{ marginRight: '2rem', width: 150 }}
          onClick={() => {
            if (status < items?.length - 1) {
              setStatus(status + 1);

              const begin = listStatus.slice(0, status + 1);
              const curr = {
                ...listStatus[status + 1],
                description: dayjs().format('YYYY/MM/DD HH:mm:ss')
              };
              const end = listStatus.slice(status + 2);

              setListStatus([...begin, curr, ...end]);
            }
          }}
        >
          {items[status]?.title}
        </Button>
      </Row>

      <Row>
        <Steps
          direction="vertical"
          current={status}
          items={listStatus}
          status={status === 8 ? 'finish' : 'process'}
        />
      </Row>
    </div>
  );
}
